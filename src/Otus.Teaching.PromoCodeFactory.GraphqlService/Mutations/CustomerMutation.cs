﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.GraphqlService.Mutations
{
    public class CustomerMutation
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerMutation(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<Customer> Create(CustomerMutationRequest request)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            var customer = MapFromRequest(request, preferences);
            await _customerRepository.AddAsync(customer);

            return customer;
        }

        public async Task<Customer> Edit(Guid id, CustomerMutationRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return null;

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            MapFromRequest(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return customer;
        }

        public async Task<Customer> Delete(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return null;

            await _customerRepository.DeleteAsync(customer);

            return customer;
        }

        private static Customer MapFromRequest(CustomerMutationRequest request, IEnumerable<Preference> preferences, Customer customer = null)
        {
            customer ??= new Customer {Id = Guid.NewGuid()};

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }
    }
}
