﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GraphqlService.Types;

namespace Otus.Teaching.PromoCodeFactory.GraphqlService.Queries
{
    public class CustomerQuery
    {
        private readonly IRepository<Customer> _customerRepository;

        public CustomerQuery(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [UsePaging(typeof(CustomerType)), UseFiltering, UseSorting]
        public async Task<IEnumerable<Customer>> Get() => await _customerRepository.GetAllAsync();
    }
}
