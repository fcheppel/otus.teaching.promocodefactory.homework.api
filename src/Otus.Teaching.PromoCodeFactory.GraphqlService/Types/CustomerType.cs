﻿using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.GraphqlService.Types
{
    public class CustomerType : ObjectType<Customer>
    {
        protected override void Configure(IObjectTypeDescriptor<Customer> descriptor)
        {
            descriptor.Field(c => c.Id).Type<IdType>();
            descriptor.Field(c => c.FirstName).Type<StringType>();
            descriptor.Field(c => c.LastName).Type<StringType>();
            descriptor.Field(c => c.Email).Type<StringType>();
        }
    }
}
