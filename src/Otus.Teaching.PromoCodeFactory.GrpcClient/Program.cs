﻿using System;
using System.Threading.Tasks;
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.GrpcService;

namespace Otus.Teaching.PromoCodeFactory.GrpcClient
{
    class Program
    {
        static async Task Main()
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Customers.CustomersClient(channel);
            var customers = await client.GetCustomersAsync(new Google.Protobuf.WellKnownTypes.Empty());
            foreach (var cm in customers.Customers)
            {
                Console.WriteLine($"{cm.Id} {cm.FirstName} {cm.LastName} {cm.Email}");
            }

            var customer = await client.GetCustomerAsync(new IdRequest
            {
                Id = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0"
            });
            Console.WriteLine($"{customer.Id} {customer.FirstName} {customer.LastName} {customer.Email}");
            foreach (var pref in customer.Preferences)
            {
                Console.WriteLine($"{pref.Id} {pref.Name}");
            }


            Console.ReadKey();
        }
    }
}
