using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GrpcService.Mappers;

namespace Otus.Teaching.PromoCodeFactory.GrpcService.Services
{
    public class CustomerService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        private const string CannotParseId = "Cannot parse id.";
        private const string CustomerNotFound = "Customer not found.";

        public CustomerService(
            IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository
            )
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomersShortResponse> GetCustomers(Empty request, ServerCallContext callContext)
        {
            var customers = await _customerRepository.GetAllAsync();

            var tempCustomers = customers.Select(x => new CustomerShortResponse
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            var response = new CustomersShortResponse();
            response.Customers.AddRange(tempCustomers);

            return response;
        }

        public override async Task<CustomerResponse> GetCustomer(IdRequest request, ServerCallContext callContext)
        {
            if (Guid.TryParse(request.Id, out var id) == false)
                throw new RpcException(new Status(StatusCode.InvalidArgument, CannotParseId));

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, CustomerNotFound));

            var response = new CustomerResponse
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };
            response
                .Preferences
                .AddRange(
                    customer
                        .Preferences
                        .Select(s =>
                            new PreferenceResponse
                            {
                                Id = s.PreferenceId.ToString(),
                                Name = s.Preference.Name
                            }
                        )
                        .ToHashSet()
                );

            return response;
        }

        public override async Task<Empty> CreateCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());
            var customer = CustomerMapper.MapFromModel(request, preferences);
            await _customerRepository.AddAsync(customer);
            return new Empty();
        }

        public override async Task<Empty> EditCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            if (Guid.TryParse(request.Id, out var id) == false)
                throw new RpcException(new Status(StatusCode.InvalidArgument, CannotParseId));
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, CustomerNotFound));
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());
            CustomerMapper.MapFromModel(request, preferences, customer);
            await _customerRepository.UpdateAsync(customer);
            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(IdRequest request, ServerCallContext context)
        {
            if (Guid.TryParse(request.Id, out var id) == false)
                throw new RpcException(new Status(StatusCode.InvalidArgument, CannotParseId));
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, CustomerNotFound));
            await _customerRepository.DeleteAsync(customer);
            return new Empty();
        }
    }
}
