﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
 {
     public class CustomerMapper
     {

         public static Customer MapFromModel(
             CreateOrEditCustomerRequest model,
             IEnumerable<Preference> preferences,
             Customer customer = null
         )
         {
             customer ??= new Customer {Id = Guid.NewGuid()};

             customer.FirstName = model.FirstName;
             customer.LastName = model.LastName;
             customer.Email = model.Email;

             customer.Preferences = preferences
                 .Select(x =>
                     new CustomerPreference
                     {
                         CustomerId = customer.Id,
                         Preference = x,
                         PreferenceId = x.Id
                     }
                 ).ToList();

             return customer;
         }
     }
 }
