﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapGromModel(GivePromoCodeRequest request, Preference preference, IEnumerable<Customer> customers) {

            var promoCode = new PromoCode
            {
                Id = Guid.NewGuid(),
                PartnerName = request.PartnerName,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(30),
                Preference = preference,
                PreferenceId = preference.Id,
                Customers = new List<PromoCodeCustomer>()
            };

            foreach (var item in customers)
            {
                promoCode.Customers.Add(new PromoCodeCustomer
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promoCode.Id,
                    PromoCode = promoCode
                });
            }

            return promoCode;
        }
    }
}
